#include "deque.hpp"
#include <iostream>
#include <cassert>

namespace matveylevinson_train3_deque {
    void init(deque *deque, std::size_t *items, std::size_t size_original) {

        std::size_t size = size_original;
        if (size > 0) {
            size--;
            size |= size >> 1;
            size |= size >> 2;
            size |= size >> 4;
            size |= size >> 8;
            size |= size >> 16;
            size |= size >> 32;
            size++;
        } else {
            size = 0;
        }

        deque->real_size = size;
        deque->size = size_original;
        deque->start = 0;
        deque->end = 0;
        deque->items = new std::size_t[size];

        for (std::size_t i = 0; i < size_original; ++i)
            deque->items[i] = items[i];
    }

    void destroy(deque *deque) {
        delete[] deque->items;
        delete deque;
    }

    void push_front(deque *deque, std::size_t element) {
        if (deque->size == deque->real_size) {
            deque->real_size *= 2;

            std::size_t* new_items = new std::size_t[deque->real_size*2];

            for (std::size_t i = 0; i < deque->size; ++i)
                new_items[i] = get(deque, i);

            delete[] deque->items;
            deque->items = new_items;
        }

        deque->size++;
        deque->start = (deque->start - 1) % deque->real_size;
        deque->items[deque->start] = element;
    }

    void push_back(deque *deque, std::size_t element) {
        if (deque->size == deque->real_size) {
            deque->real_size *= 2;

            std::size_t* new_items = new std::size_t[deque->real_size*2];

            for (std::size_t i = 0; i < deque->size; ++i)
                new_items[i] = get(deque, i);

            delete[] deque->items;
            deque->items = new_items;
        }

        deque->size++;
        deque->end = (deque->end + 1) % deque->real_size;
        deque->items[deque->start] = element;
    }

    std::size_t get(const deque *deque, std::size_t index) {
        return deque->items[(deque->start + index) % deque->real_size - 1];
    }

    void set(deque *deque, std::size_t index, std::size_t value) {
        deque->items[(deque->start + index) % deque->real_size - 1] = value;
    }

    void erase_n_front(deque *deque, std::size_t count) {
        assert(count >= deque->size);
        deque->start = (deque->start + count) % deque->real_size;
    }

    void erase_n_back(deque *deque, std::size_t count) {
        assert(count >= deque->size);
        deque->end = (deque->end - count) % deque->real_size;
    }

    std::size_t size(const deque *deque) {
        return deque->size;
    }

    bool compare(const deque *deque_1, const deque *deque_2) {
        if (size(deque_1) != size(deque_2))
            return false;
        std::size_t deque_size = size(deque_1);
        for (std::size_t i = 0; i < deque_size; ++i)
            if (get(deque_1, i) != get(deque_2, i))
                return false;
        return true;
    }

    char *print(const deque *deque, char *delimiter) {
        return delimiter;
    }
}
