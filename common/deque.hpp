#ifndef TRAIN3_DEQUE_H
#define TRAIN3_DEQUE_H

#include <cstddef>

namespace matveylevinson_train3_deque {
    struct deque {
        std::size_t start, end, size, real_size;
        std::size_t *items;
    };

    /**
     *  @brief  This function initialises the deque.
     *  @param[in,out]  deque   The deque pointer.
     *  @param[in]      items   Items to initialise with
     *  @complexity O(1)
    */
    void init(deque *deque, std::size_t *items = nullptr, std::size_t size_original=0);


    /**
     *  @brief  This function destroys the deque and frees the memory
     *  @param[in,out]  deque   The deque pointer.
     *  @complexity O(1)
     *  @note   Is owning
    */
    void destroy(deque *deque);


    /**
     *  @brief  This function inserts given element to the beginning of the deque
     *  @param[in,out]  deque   The deque pointer.
     *  @param[in]      element The element to insert.
     *  @complexity O(1)
    */
    void push_front(deque *deque, std::size_t element);


    /**
     *  @brief  This function inserts given element to the end of the deque
     *  @param[in,out]  deque   The deque pointer.
     *  @param[in]      element The element to insert.
     *  @complexity O(1)
    */
    void push_back(deque *deque, std::size_t element);


    /**
     *  @brief  This function returns an element by provided index
     *  @param[in]  deque   The deque pointer.
     *  @param[in]  index   The element index.
     *  @return     std::size_t An element with provided index
     *  @complexity O(1)
    */
    std::size_t get(deque *deque, std::size_t index);


    /**
     *  @brief  This function returns an element by provided index
     *  @param[in, out] deque   The deque pointer.
     *  @param[in]      index   The element index.
     *  @param[in]      element The value to set.
     *  @complexity O(1)
    */
    void set(deque *deque, std::size_t index, std::size_t value);


    /**
     *  @brief  This function deletes N first elements from deque
     *  @param[in,out]  deque   The deque pointer.
     *  @param[in]      count   Number of elements to be deleted.
     *  @complexity O(n)    n = count
     *  @warning    Elements should exist
     *  @note   Is owning
     *
     *  Warning: the presence of elements is not checked. Deleting of not existing
     *  elements may lead to undefined behaviour
    */
    void erase_n_front(deque *deque, std::size_t count);


    /**
     *  @brief  This function deletes N last elements from deque
     *  @param[in,out]  deque   The deque pointer.
     *  @param[in]      count   Number of elements to be deleted.
     *  @complexity O(n)    n = count
     *  @warning    Elements should exist
     *  @note   Is owning
     *
     *  Warning: the presence of elements is not checked. Deleting of not existing
     *  elements may lead to undefined behaviour
    */
    void erase_n_back(deque *deque, std::size_t count);


    /**
     *  @brief  This function returns quantity of deque elements
     *  @param[in]  deque   The deque pointer.
     *  @return     std::size_t  Size the deque
     *  @complexity O(1)
    */
    std::size_t size(const deque *deque);


    /**
     *  @brief  This function compares 2 deques
     *  @param[in]  deque_1 The first deque pointer.
     *  @param[in]  deque_2 The second deque pointer.
     *  @complexity O(n)    n = min(deque_1 size, deque_2 size) + 1
     *  @return     bool    <true> if equal, <false> otherwise
    */
    bool compare(const deque *deque_1, const deque *deque_2);


    /**
     *  @brief  This function prints deque contents
     *  @param[in]  deque       The deque pointer.
     *  @param[in]  delimiter   The string to use as delimiter.
     *  @complexity O(n)    n = deque size
     *  @return     char[]  String with deque elements joined with the delimiter
    */
    char *print(const deque *deque, char *delimiter);
}
#endif //TRAIN3_DEQUE_H
