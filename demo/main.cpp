#include <iostream>
#include "../common/deque.hpp"

int main() {
    std::size_t current_wagon_number = 1;

    char action;
    char direction;
    std::size_t amount;

    char input;
    std::size_t initial_train_length;

    matveylevinson_train3_deque::deque train;
    matveylevinson_train3_deque::init(train);

    std::cout << "Hello, enter initial train length\n";
    if (!(std::cin >> initial_train_length)) {
        std::cerr << "An input error has happened\n";
        return 1;
    };
    for (std::size_t i = 0; i < initial_train_length; ++i) {
        matveylevinson_train3_deque::push_back(&train, current_wagon_number);
        ++current_wagon_number;
    }

    while (1) {
        if (!(std::cin >> action)) {
            std::cerr << "An input error has happened\n";
            return 1;
        };
        if (action != '+' && action != '-') {
            std::cerr << "Action must be one of '+', '-' or 'x' (for quitting) \n";
            return 2;
        }

        if (!(std::cin >> direction)) {
            std::cerr << "An input error has happened\n";
            return 1;
        };
        if (direction != '>' && direction != '<') {
            std::cerr << "Direction must be one of '<' or '>' \n";
            return 2;
        }

        if (!(std::cin >> amount)) {
            std::cerr << "An input error has happened\n";
            return 1;
        };

        switch (action) {
            case '+':
                if (direction == '>') {
                    for (std::size_t i = 0; i < amount; ++i) {
                        matveylevinson_train3_deque::push_front(&train, current_wagon_number);
                        ++current_wagon_number;
                    }
                } else {
                    for (std::size_t i = 0; i < amount; ++i) {
                        matveylevinson_train3_deque::push_back(&train, current_wagon_number);
                        ++current_wagon_number;
                    }
                }
                break;
            case '-':
                if (direction == '>')
                    matveylevinson_train3_deque::erase_n_front(&train, amount);
                else
                    matveylevinson_train3_deque::erase_n_back(&train, amount);
                break;
            case 'x':
                matveylevinson_train3_deque::print(&train, ' ');
                return 0;
            default:
                std::cerr << "Operation is not defined\n";
                break;
        }
        matveylevinson_train3_deque::print(&train, ' ');
    }
}