#include "../common/deque.hpp"
#include <cassert>
#include <iostream>
#include <random>

int uniform_random(int min, int max) {
    static std::mt19937 prng(std::random_device{}());
    return std::uniform_int_distribution<>(min, max)(prng);
}

int main() {
    {
        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, nullptr, 0);
        assert(matveylevinson_train3_deque::size(&a) == 0);
        matveylevinson_train3_deque::destroy(&a);
    }
//Добавление элементов в начало/конец пустого/не пустого дека.
    {
        const std::size_t test_number = uniform_random(0, 1000);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, nullptr, 0);
        matveylevinson_train3_deque::push_front(&a, test_number);
        assert(matveylevinson_train3_deque::get(&a, 0) == test_number);
        matveylevinson_train3_deque::destroy(&a);
    }
    {
        const size_t test_number = uniform_random(0, 1000);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, nullptr, 0);
        matveylevinson_train3_deque::push_back(&a, test_number);
        assert(matveylevinson_train3_deque::get(&a, 0) == test_number);
        matveylevinson_train3_deque::destroy(&a);
    }
    {
        const std::size_t test_initial_numbers_size = uniform_random(0, 10);
        std::size_t * test_initial_numbers = new std::size_t[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_number = uniform_random(0, 1000);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::push_front(&a, test_number);
        assert(matveylevinson_train3_deque::get(&a, 0) == test_number);
        matveylevinson_train3_deque::destroy(&a);
        delete[] test_initial_numbers;
    }
    {
        const std::size_t test_initial_numbers_size = uniform_random(0, 10);
        std::size_t * test_initial_numbers = new std::size_t[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_number = uniform_random(0, 1000);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::push_back(&a, test_number);
        assert(matveylevinson_train3_deque::get(&a, sizeof(test_initial_numbers)) == test_number);
        matveylevinson_train3_deque::destroy(&a);
        delete[] test_initial_numbers;
    }
//Удаление всех/не всех элементов с начала/конца.
    {
        const std::size_t test_initial_numbers_size = uniform_random(0, 10);
        std::size_t * test_initial_numbers = new std::size_t[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_delete_count = uniform_random(1, test_initial_numbers_size-1);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::erase_n_front(&a, test_delete_count);
        assert(matveylevinson_train3_deque::size(&a) == test_initial_numbers_size-test_delete_count);
        //TODO: check elements
        matveylevinson_train3_deque::destroy(&a);
        delete[] test_initial_numbers;
    }
    {
        const std::size_t test_initial_numbers_size = uniform_random(0, 10);
        std::size_t * test_initial_numbers = new std::size_t[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_delete_count = uniform_random(1, test_initial_numbers_size-1);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::erase_n_back(&a, test_delete_count);
        assert(matveylevinson_train3_deque::size(&a) == test_initial_numbers_size-test_delete_count);
        //TODO: check elements
        matveylevinson_train3_deque::destroy(&a);
        delete[] test_initial_numbers;
    }
    {
        const std::size_t test_initial_numbers_size = uniform_random(0, 10);
        std::size_t * test_initial_numbers = new std::size_t[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_delete_count = test_initial_numbers_size;

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::erase_n_front(&a, test_delete_count);
        assert(matveylevinson_train3_deque::size(&a) == test_initial_numbers_size-test_delete_count);
        //TODO: check elements
        matveylevinson_train3_deque::destroy(&a);
        delete[] test_initial_numbers;
    }
    {
        const std::size_t test_initial_numbers_size = uniform_random(0, 10);
        std::size_t * test_initial_numbers = new std::size_t[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_delete_count = test_initial_numbers_size;

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::erase_n_back(&a, test_delete_count);
        assert(matveylevinson_train3_deque::size(&a) == test_initial_numbers_size-test_delete_count);
        //TODO: check elements
        matveylevinson_train3_deque::destroy(&a);
        delete[] test_initial_numbers;
    }
//Добавление в начало/конец, приводящее к перевыделению динамической памяти
    {
        const std::size_t test_initial_numbers_size = 8;
        std::size_t test_initial_numbers[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_number = uniform_random(0, 1000);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::push_front(&a, test_number);

        assert(matveylevinson_train3_deque::size(&a) == test_initial_numbers_size+1);
        assert(matveylevinson_train3_deque::get(&a, 0) == test_number);
        assert(matveylevinson_train3_deque::get(&a, test_initial_numbers_size) == test_initial_numbers[test_initial_numbers_size-1]);

        matveylevinson_train3_deque::destroy(&a);
    }
    {
        const std::size_t test_initial_numbers_size = 8;
        std::size_t test_initial_numbers[test_initial_numbers_size];
        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
            test_initial_numbers[i] = uniform_random(0, 1000);
        }
        const std::size_t test_number = uniform_random(0, 1000);

        matveylevinson_train3_deque::deque a;
        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
        matveylevinson_train3_deque::push_back(&a, test_number);

        assert(matveylevinson_train3_deque::size(&a) == test_initial_numbers_size+1);
        assert(matveylevinson_train3_deque::get(&a, test_initial_numbers_size) == test_number);
        assert(matveylevinson_train3_deque::get(&a, 0) == test_initial_numbers[0]);

        matveylevinson_train3_deque::destroy(&a);
    }
//Цепочка удалений/вставок, приводящая к обратному, относительно изначального, положению указателей начала/конца.

//    {
//        const size_t test_initial_numbers_size = 8;
//        size_t test_initial_numbers[test_initial_numbers_size];
//        for (std::size_t i = 0; i < test_initial_numbers_size; ++i) {
//            test_initial_numbers[i] = uniform_random(0, 1000);
//        }
//        const std::size_t test_delete_count = test_initial_numbers_size;
//        const std::size_t test_number = uniform_random(0, 1000);

//        matveylevinson_train3_deque::deque a;
//        matveylevinson_train3_deque::init(&a, test_initial_numbers, sizeof(test_initial_numbers)/sizeof( *test_initial_numbers));
//        matveylevinson_train3_deque::push_front(&a, test_number);

//        assert(matveylevinson_train3_deque::size(&a) == test_initial_numbers_size+1);
//        assert(matveylevinson_train3_deque::get(&a, 0) == test_number);
//        assert(matveylevinson_train3_deque::get(&a, test_initial_numbers_size) == test_initial_numbers[test_initial_numbers_size-1]);

//        matveylevinson_train3_deque::destroy(&a);
//    }
}
